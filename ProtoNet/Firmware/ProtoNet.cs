﻿using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using ProtoNet.Config;
using ProtoNet.Data;
using System.Text;

namespace ProtoNet
{
    /// <summary>
    /// A prototype device using C# and .Net with basic functionalities for
    /// connecting to an IoT Hub, sending telemetry, and receiving a remote
    /// method call from the hub
    /// </summary>
    public class ProtoNet : IDisposable
    {
        private readonly DeviceSettings _settings;

        private DeviceClient? _client { get; set; }
        private TimeSpan? _telemetryInterval { get; set; }

        public ProtoNet(DeviceSettings settings)
        {
            _settings = settings;
            _telemetryInterval = TimeSpan.FromSeconds(settings.ReportingIntervalSeconds);

            InitializeIoTClientAsync().Wait();
            InitializeDevice().Wait();
        }

        /// <summary>
        /// Disposes of the DeviceClient created during initialization
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Dispose()
        {
            if (_client != null)
            {
                _client.CloseAsync().Wait();
                _client.Dispose();

                Console.WriteLine("IoT Client successfully disposed");
            }
        }

        /// <summary>
        /// Creates a new DeviceClient and sets a handler for the "SetTelemetryInterval"
        /// remote function
        /// </summary>
        /// <returns></returns>
        private async Task InitializeIoTClientAsync()
        {
            Console.WriteLine("Establishing connection to IoT Hub...");

            _client = DeviceClient.CreateFromConnectionString(_settings?.IoTHub?.ConnectionString);
            await _client.SetMethodHandlerAsync("SetTelemetryInterval", HandleSetTelemetryInterval, null);

            Console.WriteLine("Completed!");
        }

        /// <summary>
        /// Sends an initialization message to the IoT Hub containing the
        /// device's definition (information)
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task InitializeDevice()
        {
            if (_settings == null)
                throw new ApplicationException("Invalid Settings");

            if (_client == null)
                throw new ApplicationException("Invalid Client");

            string messageBody = JsonConvert.SerializeObject(new ServiceBusMessage
            {
                Source = _settings.DeviceDefinition.DeviceId,
                Assembly = "FarmManager.Data",
                Class = "FarmManager.Data.Entities.Devices.Device",
                MessageType = "Telemetry.RegisterDevice",
                TimeStamp = DateTimeOffset.UtcNow,
                Data = _settings.DeviceDefinition
            });

            Message message = new Message(Encoding.ASCII.GetBytes(messageBody));
            await _client.SendEventAsync(message);
        }

        /// <summary>
        /// Handler for when the ProtoNet receives a message from the IoT Hub for 
        /// adjusting the interval at which telemetry should be sent
        /// </summary>
        /// <param name="methodRequest"></param>
        /// <param name="userContext"></param>
        /// <returns></returns>
        private Task<MethodResponse> HandleSetTelemetryInterval(MethodRequest methodRequest, object userContext)
        {
            try
            {
                string data = Encoding.UTF8.GetString(methodRequest.Data);
                SetTelemetryParameters? parameters = JsonConvert.DeserializeObject<SetTelemetryParameters>(data);

                // Check that the payload is a single integer value
                if (parameters != null)
                {
                    _telemetryInterval = TimeSpan.FromSeconds(parameters.Interval);

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Telemetry interval set to {_telemetryInterval}");
                    Console.ResetColor();

                    // Acknowlege the direct method call with a 200 success message.
                    string result = JsonConvert.SerializeObject(new DirectMethodResult
                    {
                        Success = true
                    });
                    return Task.FromResult(new MethodResponse(Encoding.UTF8.GetBytes(result), 200));
                }
                else
                {
                    // Acknowlege the direct method call with a 400 error message.
                    string result = JsonConvert.SerializeObject(new DirectMethodResult
                    {
                        Success = false,
                        Message = "Invalid Parameter"
                    });
                    return Task.FromResult(new MethodResponse(Encoding.UTF8.GetBytes(result), 400));
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Error changing telemetry interval: {ex.Message}");
                string result = JsonConvert.SerializeObject(new DirectMethodResult
                {
                    Success = false,
                    Message = ex.Message
                });
                return Task.FromResult(new MethodResponse(Encoding.UTF8.GetBytes(result), 400));
            }
        }

        /// <summary>
        /// The main program of the ProtoNet
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Run(CancellationToken cancellationToken)
        {
            if (_telemetryInterval == null)
                throw new ApplicationException("Invalid telemetry interval");

            Random rand = new Random();

            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (_client != null)
                    {
                        string messageBody = GenerateTelemetryData(rand);
                        Message message = new Message(Encoding.ASCII.GetBytes(messageBody));
                        await _client.SendEventAsync(message);
                        Console.WriteLine($"Telemetry Delivered!");
                    }
                    else
                    {
                        Console.WriteLine("Client not available...");
                    }

                    await Task.Delay((TimeSpan)_telemetryInterval, cancellationToken);
                }
            }
            catch (TaskCanceledException) 
            {
                Dispose();
            }
        }

        /// <summary>
        /// A helper function to generate random data that can be submitted to the
        /// IoT Hub as telemetry
        /// </summary>
        /// <param name="rand"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private string GenerateTelemetryData(Random rand)
        {
            if (_settings == null)
                throw new ApplicationException("Invalid Settings");

            // Generate random telemetry data based on the interface's name
            var interfaceData = _settings.DeviceDefinition.Interfaces.Select(deviceInterface =>
            {
                object telemetryData = new();

                if (deviceInterface.InterfaceId.Contains("Sensor"))
                {
                    telemetryData = new
                    {
                        Temperature = rand.NextDouble() * 15,
                        Humidity = rand.NextDouble() * 20
                    };
                }
                else if (deviceInterface.InterfaceId.Contains("Switch"))
                {
                    telemetryData = new
                    {
                        Value = rand.NextSingle() > .5
                            ? true
                            : false
                    };
                }

                return new
                {
                    Interface = deviceInterface.InterfaceId,
                    Telemetry = telemetryData
                };
            });

            return JsonConvert.SerializeObject(new ServiceBusMessage
            {
                Source = _settings.DeviceDefinition.DeviceId,
                MessageType = "Telemetry.Reading",
                TimeStamp = DateTimeOffset.UtcNow,
                Data = interfaceData
            });
        }
    }
}
