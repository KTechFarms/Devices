﻿namespace ProtoNet.Config
{
    /// <summary>
    /// Settings for the device, used to connect to the 
    /// IoT hub and communicate with the Farm Manager system
    /// </summary>
    public class DeviceSettings
    {
        /// <summary>
        /// How often the device will submit telemetry readings to
        /// Farm Manager
        /// </summary>
        public int ReportingIntervalSeconds { get; set; }

        /// <summary>
        /// The representation of the device, sent to Farm Manager
        /// when the device is first powered on
        /// </summary>
        public DeviceDefinition DeviceDefinition { get; set; }

        /// <summary>
        /// Settings related to the IoT Hub
        /// </summary>
        public IoTHubSettings IoTHub { get; set; }
    }
}
