﻿namespace ProtoNet.Data
{
    /// <summary>
    /// A typical message sent to the service bus
    /// </summary>
    public class ServiceBusMessage
    {
        /// <summary>
        /// The source of the message
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// The type of message
        /// </summary>
        public string MessageType { get; set; }

        /// <summary>
        /// The assembly of the Data object
        /// </summary>
        public string Assembly { get; set; }

        /// <summary>
        /// The class of the Data object
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// The timestamp at which the message was created
        /// </summary>
        public DateTimeOffset TimeStamp { get; set; }

        /// <summary>
        /// An object representing the data of the message
        /// </summary>
        public object Data { get; set; }
    }
}
