import network
import machine
import json
from random import randint
from utime import sleep, localtime
from umqtt.simple import MQTTClient

import secrets
from device import definition

#https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-mqtt-support
user_name = f'{secrets.HUB_HOST}/{secrets.DEVICE_ID}/?api-version=2021-04-12'
topic_pub = f'devices/{secrets.DEVICE_ID}/messages/events/'
subscribe_topic = f'devices/{secrets.DEVICE_ID}/messages/devicebound/#'
port_no = 0


def connect_to_wireless():
    print("Starting wireless")
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)

    while not wlan.isconnected():
        wlan.connect(secrets.SSID, secrets.PASSWORD)
        print(f"Connected: {wlan.isconnected()}")
        sleep(2)
        
    print("Connected!")


def mqtt_connect():
    #https://github.com/Azure/azure-iot-sdk-c/blob/main/certs/certs.c
    certificate_path = "baltimore.cer"
    print('Loading Blatimore Certificate')
    with open(certificate_path, 'r') as f:
        cert = f.read()
    print('Obtained Baltimore Certificate')
    sslparams = {'cert':cert}
    
    client = MQTTClient(client_id=secrets.DEVICE_ID, server=secrets.HUB_HOST, port=port_no, user=user_name, password=secrets.HUB_PASS, keepalive=3600, ssl=True, ssl_params=sslparams)
    client.connect()
    print('Connected to IoT Hub MQTT Broker')
    return client


def reconnect():
    print('Failed to connect to the MQTT Broker. Reconnecting...')
    sleep(5)
    machine.reset()


def callback_handler(topic, message_receive):
    print("Received message")
    print(topic)
    print(message_receive)
    

def send_telemetry():
    current_time = localtime()
    topic_msg = json.dumps({
        "Source": "prototype",
        "MessageType": "Telemetry",
        "Assembly": "FarmManager.Data",
        "Class": "FarmManager.Data.ServiceBusMessages.TelemetryMessage",
        "TimeStamp": f"{current_time[0]}-{current_time[1]}-{current_time[2]} {current_time[3]}:{current_time[4]}:{current_time[5]}",
        "Data": [
            {                
                "Interface": definition["Interfaces"][0]["Id"],
                "Telemetry": {
                    "Temperature": randint(60, 100),
                    "Humidity": randint(25, 40)
                }
            }
        ]
    })
    client.publish(topic_pub, topic_msg)
    print("Published!")


try:
    connect_to_wireless()
    client = mqtt_connect()
    client.set_callback(callback_handler)
    client.subscribe(topic=subscribe_topic)
except OSError as e:
    reconnect()

send_telemetry()

while True:
    client.check_msg()
    print('waiting...')
    sleep(1)