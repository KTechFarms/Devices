definition = {
    "DeviceId": "prototype",
    "Name": "prototype",
    "Model": "ProtoType_1",
    "SerialNumber": "0000001",
    "Interfaces": [
        {
            "Id": "Sensor 1",
            "Type": {
                "Model": "DHT11",
                "Description": "A temperature and humidity sensor",
            },
        }
    ]
}