﻿namespace ProtoNet.Config
{
    /// <summary>
    /// The type of interface being represented, such as
    /// a DHT11
    /// </summary>
    public class InterfaceType
    {
        /// <summary>
        /// The model of the interface
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// A description of the Interface
        /// </summary>
        public string Description { get; set; }
    }

    /// <summary>
    /// Representation of a method that can be executed remotely
    /// </summary>
    public class DeviceMethod
    {
        /// <summary>
        /// The name of the method
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of the method
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A json representation of parameters that are accepted
        /// by the method
        /// </summary>
        public string Parameters { get; set; }
    }

    /// <summary>
    /// Representation of an interface that interacts with the 
    /// physical world, such as a switch or sensor
    /// </summary>
    public class Interface
    {
        /// <summary>
        /// The name of the interface
        /// </summary>
        public string InterfaceId { get; set; }

        /// <summary>
        /// The type of interface that's being represented
        /// </summary>
        public InterfaceType InterfaceType { get; set; }
    }

    /// <summary>
    /// A representation of a Farm Manager device 
    /// </summary>
    public class DeviceDefinition
    {
        /// <summary>
        /// A unique Identifier for the device
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// A unique key used as a password for the device
        /// </summary>
        public string DeviceKey { get; set; }

        /// <summary>
        /// A user friendly name for the device
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The model of the device
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// The serial number of the device
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// A list of interfaces that the device has access to
        /// </summary>
        public List<Interface> Interfaces { get; set; }

        /// <summary>
        /// A list of methods that can be called remotely
        /// </summary>
        public List<DeviceMethod> Methods { get; set; }
    }
}
