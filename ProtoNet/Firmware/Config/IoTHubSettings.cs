﻿namespace ProtoNet.Config
{
    /// <summary>
    /// Settings specific to the IoT Hub 
    /// </summary>
    public class IoTHubSettings
    {
        /// <summary>
        /// The device-specific connection string to the IoT Hub
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
