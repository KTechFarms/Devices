﻿using Microsoft.Extensions.Configuration;
using ProtoNet.Config;

namespace ProtoNet
{
    public class Program
    {
        private static DeviceSettings? _settings { get; set; }

        public static async Task Main(string[] args)
        {
            LoadSettings();

            if (_settings == null)
                throw new ApplicationException("Device settings failed to load!");

            ProtoNet protoNet = new ProtoNet(_settings);

            using (CancellationTokenSource cts = new CancellationTokenSource())
            {
                CreateCancellationEvent(cts);
                await protoNet.Run(cts.Token);
            }
        }

        /// <summary>
        /// Loads the device-specific settings from device.{ENVIRONMENT}.json, as
        /// determined by the ASPNETCORE_ENVIRONMENT environment variable
        /// </summary>
        private static void LoadSettings()
        {
            Console.WriteLine("Loading Settings...");

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"device.{env}.json");

            _settings = new ();
            IConfiguration config = builder.Build();
            config.GetSection("DeviceSettings").Bind(_settings);

            Console.WriteLine("Completed!");
        }

        /// <summary>
        /// Creates a cancellation event for the ctrl+c key combination that will trigger
        /// shutting down the device
        /// </summary>
        /// <param name="cancellationTokenSource"></param>
        private static void CreateCancellationEvent(CancellationTokenSource cancellationTokenSource)
        {
            Console.WriteLine("Press control-c to exit.");
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                eventArgs.Cancel = true;
                cancellationTokenSource.Cancel();
                Console.WriteLine("Exiting...");
            };
        }
    }
}
    