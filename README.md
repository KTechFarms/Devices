# KTech Farms - Devices

The Devices repository is a collection of firmware for the various farm devices utilized and sold by KTech Farms.

## Device List

| Category | Device Name | Description |
| -------- | ----------- | ----------- |
| Prototype | ProtoNet   | A .Net console based device for easy testing and validation |
| Prototype | ProtoPico  | A prototype with basic functionality mirroring the ProtoNet, but built on a Raspberry Pi Pico |
| Prototype | ProtoTower | A hydroponic tower prototype that consists of 20 towers with a capacity of 180 plants |

## Directory Structure

Each directory in the repo contains all of the files needed to create and build the selected device. The folder is the name of the device and will conform to the following structure:

```
- README.md
- Firmware
    - main.py
    - secrets.py
    - device.py
    - lib
        - lib-1
        - lib-2
- Design
    - <DeviceName>.f3d
    - <PartName-1>.stl
    - <PartName-2>.stl
```

### README.md

The readme file will contain basic information about the device and instructions about how to get started

### Firmware

The firmware directory will contain the files needed for the device to function, and should be copied to the root of the Raspberry pi pico

- main.py is the entry point of the program, and starts when the device powers on
- secrets.py contains sensitive information used by the device, such as the network SSID and password
- device.py contains the definition of the device, which is used by the Farm Manager system to communicate with and store data from the device
- The lib directory contains library files needed for the device to function. These could libraries created by KTech Farms or other individuals

### Design

The design directory will contain any files needed to build the device, such as 3d models and circuit diagrams

- {DeviceName}.f3d is the device's Autodest Fusion 360 file
- {PartName-X}.stl files are any 3d models of the device's parts that can be 3d-printed
