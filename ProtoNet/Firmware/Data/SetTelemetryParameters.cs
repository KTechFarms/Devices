﻿namespace ProtoNet.Data
{
    public class SetTelemetryParameters
    {
        public int Interval { get; set; }
    }
}
