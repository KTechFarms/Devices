﻿namespace ProtoNet.Data
{
    public class DirectMethodResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
