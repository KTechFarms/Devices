# Prototype

The Prototype device is a basic device that is used to test and verify things are working between a Raspberry Pi Pico and the Azure IoT Hub.

## Bill of Materials

- (1) Raspberry Pi Pico W
- (1) Breadboard
- (1) Micro USB cable

## Prerequisites

1. Install [Thonny](https://thonny.org/)
2. Setup an Azure account and IoT Hub by following the instructions on the [Farm Manager Wiki](https://gitlab.com/KTechFarms/Farm-Manager/-/wikis/Installation)
3. Register a prototype device within the IoT Hub

```
az iot hub device-identity create -d {device name} -n {iot hub name}
```

4. Install [Azure IoT Explorer](https://docs.microsoft.com/en-us/azure/iot-fundamentals/howto-use-iot-explorer) and connect to the Azure IoT Hub
5. Using Azure IoT Explorer, generate a SAS token for the connected device. This will be used as the HUB_PASS value in the "Getting Started" section

5. (Optional) Begin monitoring the IoT hub for events

```
az iot hub monitor-events -n {iot hub name}
```

## Getting Started

1. Hold down the boot select and plug the pico into your computer
2. Install Micropython by dragging the [UF2 file](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython) onto the drive
3. Open Thonny and connect to the pico
4. Create a secrets.py file that contains the following:

```python
SSID="WIRELESS_SSID"
PASSWORD="WIRELESS_PASSWORD"
HUB_HOST="IOT_HUB_FQDN"
DEVICE_ID="IOT_HUB_DEVICE_ID"
HUB_PASS="IOT_HUB_DEVICE_SAS"
```

Example values may look like:

```python
SSID="MyWirelessSSID"
PASSWORD="MyWirelessPassword"
HUB_HOST="MyHub.azure.devices.net"
DEVICE_ID="MyClientIdOrName"
HUB_PASS="SharedAccessSignature sr=MyHub.azure-devices.net%2Fdevices%2FMyClientIdOrName&sig=SomeSignature"
```

5. Copy the main.py and baltimore.cer files as well as the lib directory to the root of the pico using Thonny
7. Open the pico's main.py file from within Thonny
8. Click the green play / start button in Thonny's toolbar. You should see the pico connect to wireless, the IoT Hub, and publish a message